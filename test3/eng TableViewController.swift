//
//  eng TableViewController.swift
//  test3
//
//  Created by apple on 2017/4/14.
//  Copyright © 2017年 apple. All rights reserved.
//

import UIKit
import AVFoundation
import  CoreData

class eng_TableViewController: UITableViewController {
    var mycontext : NSManagedObjectContext!
    var english: [EnglishMO] = []
    
    func getContext () -> NSManagedObjectContext{
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    func storeEnglish(){
    let context = getContext()
        var english:EnglishMO!
        english = EnglishMO(context: getContext())
        english.name = "earnings"
        english.cht = "薪水"
        english.speak = "ˋɝnɪŋz"
        if let englishImage = UIImage(named:"earnings.jpg"){
        if let imageData = UIImageJPEGRepresentation(englishImage, 1.0)
        {
            english.picture = NSData(data:imageData )
            
            }
        }
        
        do {
        try context.save()
            print("success")
            
        } catch {
        print(error)
        
        }
        
    }
    func getWord() {
        let request:NSFetchRequest<EnglishMO> = EnglishMO.fetchRequest()
        let context = getContext()
        do {
            english = try context.fetch(request)
            print(english.count)
            for word in english {
                print(word.name!)
                print(word.cht!)
                print(word.speak!)
                
            }
        } catch  {
            print(error)
        }
    }
    func delCoreData() {
        let request:NSFetchRequest<EnglishMO> = EnglishMO.fetchRequest()
        let context = getContext()
        do {
            english = try context.fetch(request)
            print(english.count)
            for word in english {
                print(word.name!)
                print(word.cht!)
                print(word.speak!)
                
            }
        } catch  {
            print(error)
        }
    }

    var englishs : [English] = [
        English(name:"abacus",cht:"算盤", speak:"ˋæbəkəs", picture:"abacus.jpg"),
        English(name:"abbey",cht:"修道院", speak:"ˋæbɪ", picture:"abbey.jpg"),
        English(name:"ache",cht:"痛", speak:"ek", picture:"ache.jpg"),
        English(name:"acid",cht:"酸", speak:"ˋæsɪd", picture:"acid.jpg"),
        English(name:"actor",cht:"男演員", speak:"ˋæktɚ", picture:"actor.png"),
        English(name:"actress",cht:"女演員", speak:"ˋæktrɪs", picture:"actress.jpg"),
        English(name:"aim",cht:"瞄準", speak:"em", picture:"aim.png"),
        English(name:"air",cht:"空氣", speak:"ɛr", picture:"air.jpg"),
        English(name:"airplane",cht:"飛機", speak:"ˋɛr͵plen", picture:"airplane.jpg"),
        English(name:"aisle",cht:"走廊", speak:"aɪl", picture:"aisle.jpg"),
        English(name:"album",cht:"相簿", speak:"ˋælbəm", picture:"album.jpg"),
        English(name:"albumen",cht:"蛋白", speak:"ælˋbjumən", picture:"albumen.png"),
        English(name:"alcohol",cht:"酒精", speak:"ˋælkə͵hɔl", picture:"alcohol.jpg"),
        English(name:"alcoholic",cht:"酒鬼", speak:"͵ælkəˋhɔlɪk", picture:"alcoholic.jpg"),
        English(name:"alien",cht:"外星人", speak:"ˋelɪən", picture:"alien.jpg"),
        English(name:"all",cht:"全部", speak:"ɔl", picture:"all.jpg"),
        English(name:"alley",cht:"小巷", speak:"ˋælɪ", picture:"alley.jpg"),
        English(name:"alveolus",cht:"肺泡", speak:"ælˋviələs", picture:"alveolus.jpg"),
        English(name:"amazon",cht:"女戰士", speak:"ˋæmə͵zɑn", picture:"amazon.jpg"),
        English(name:"amethyst",cht:"紫水晶", speak:"ˋæməθɪst", picture:"amethyst.jpg"),
        English(name:"apple",cht:"蘋果", speak:"ˋæp!", picture:"apple.jpg"),
        English(name:"approval",cht:"贊同", speak:"əˋpruv!", picture:"approval.jpg"),
        English(name:"argument",cht:"爭執", speak:"ˋɑrgjəmənt", picture:"argument.jpg"),
        English(name:"arms",cht:"武器", speak:"ɑrmz", picture:"arms.jpg"),
        English(name:"ash",cht:"灰燼", speak:"æʃ", picture:"ash.jpg"),
        English(name:"assassin",cht:"暗殺者", speak:"əˋsæsɪn", picture:"assassin.jpg"),
        English(name:"attack",cht:"攻擊", speak:"əˋtæk", picture:"attack.png"),
        English(name:"awning",cht:"遮雨棚", speak:"ˋɔnɪŋ", picture:"awning.jpg"),
        English(name:"axe",cht:"斧頭", speak:"æks", picture:"axe.jpg"),
        English(name:"azalea",cht:"杜鵑花", speak:"əˋzeljə", picture:"azalea.jpg"),
        English(name:"Backward",cht:"向後", speak:"ˈbakwərd", picture:"backward.jpg"),
        English(name:"Bacteria",cht:"病菌", speak:"ˌbakˈtirēəm", picture:"bacteria.jpg"),
        English(name:"Badge",cht:"徽章", speak:"baj", picture:"badge.jpg"),
        English(name:"Badminton",cht:"羽毛球", speak:"ˈbadmin(tn", picture:"badminton.jpg"),
        English(name:"Baggage",cht:"行李", speak:"ˈbaɡij", picture:"baggage.jpg"),
        English(name:"Bait",cht:"誘餌", speak:"bāt", picture:"bait.jpg"),
        English(name:"Bamboo",cht:"竹子", speak:"bamˈbo͞o", picture:"bamboo.jpg"),
        English(name:"Barbarian",cht:"野蠻人", speak:"ˌbärˈberēən", picture:"barbarian.jpg"),
        English(name:"Barbecue",cht:"烤肉", speak:"ˈbärbəˌkyo͞o", picture:"barbecue.jpg"),
        English(name:"Barber",cht:"理髮師", speak:"ˈbärbər", picture:"barber.jpg"),
        English(name:"Bartender",cht:"調酒師", speak:"bärˌtendər", picture:"bartender.jpg"),
        English(name:"Basement",cht:"地下室", speak:"ˈbāsmənt", picture:"basement.jpg"),
        English(name:"Basketball",cht:"籃球", speak:"ˋbæskɪt͵bɔl", picture:"basketball.jpg"),
        English(name:"Beautiful",cht:"漂亮的", speak:"ˈbyo͞odəfəl", picture:"beautiful.jpg"),
        English(name:"Bedroom",cht:"臥室", speak:"ˈbedˌro͞om", picture:"bedroom.jpg"),
        English(name:"Beetle",cht:"甲蟲", speak:"ˈbēdl", picture:"beetle.jpg"),
        English(name:"Beverage",cht:"飲料", speak:"ˈbev(ərij", picture:"beverage.jpg"),
        English(name:"Binoculars",cht:"雙筒望遠鏡", speak:"bəˈnäkyələrz", picture:"binoculars.jpg"),
        English(name:"Biochemistry",cht:"化學", speak:"ˌbīōˈkeməstrē", picture:"biochemistry.jpg"),
        English(name:"Birthday",cht:"生日", speak:"ˈbərTHˌdā", picture:"birthday.jpg"),
        English(name:"Blaze",cht:"火焰", speak:"blāz", picture:"blaze.jpg"),
        English(name:"Bodyguard",cht:"保鏢", speak:"bädēˌɡärd", picture:"bodyguard.jpg"),
        English(name:"Boxer",cht:"拳擊手", speak:"ˈbäksər", picture:"boxer.jpg"),
        English(name:"Braid",cht:"辮子", speak:"brād", picture:"braid.jpg"),
        English(name:"Bride",cht:"新娘", speak:"brīd", picture:"bride.jpg"),
        English(name:"Bulb",cht:"燈泡", speak:"bəlb", picture:"bulb.jpg"),
        English(name:"Burglar",cht:"闖空門的人", speak:"ˈbərɡlər", picture:"burglar.jpg"),
        English(name:"Burial",cht:"墳墓", speak:"ˈberēəl", picture:"burial.jpg"),
        English(name:"Butcher",cht:"肉販", speak:"ˈbo͝oCHər", picture:"butcher.jpg"),
        English(name:"Butterfly",cht:"蝴蝶", speak:"ˋbʌtɚ͵flaɪ", picture:"butterfly.jpg"),
        English(name:"cabbage",cht:"大白菜", speak:"ˋkæbɪdʒ", picture:"cabbage.jpg"),
        English(name:"cabin",cht:"船艙", speak:"ˋkæbɪn", picture:"cabin.jpg"),
        English(name:"cactus",cht:"仙人掌", speak:"ˋkæktəs", picture:"cactus.jpg"),
        English(name:"cadaver",cht:"屍體", speak:"kəˋdævɚ", picture:"cadaver.jpg"),
        English(name:"cake",cht:"蛋糕", speak:"kek", picture:"cake.jpg"),
        English(name:"camp",cht:"營地", speak:"kæmp", picture:"camp.jpg"),
        English(name:"can",cht:"罐頭", speak:"kæn", picture:"can.jpg"),
        English(name:"candle",cht:"蠟燭", speak:"ˋkænd!", picture:"candle.jpg"),
        English(name:"candy",cht:"糖果", speak:"ˋkændɪ", picture:"candy.jpg"),
        English(name:"cane",cht:"手杖", speak:"ken", picture:"cane.jpg"),
        English(name:"canyon",cht:"峽谷", speak:"ˋkænjən", picture:"canyon.jpg"),
        English(name:"cap",cht:"帽子", speak:"kæp", picture:"cap.jpg"),
        English(name:"cape",cht:"斗篷", speak:"kep", picture:"cape.jpg"),
        English(name:"carrot",cht:"胡蘿蔔", speak:"ˋkærət", picture:"carrot.jpg"),
        English(name:"case",cht:"盒子", speak:"kes", picture:"case.jpg"),
        English(name:"cash",cht:"現金", speak:"kæʃ", picture:"cash.jpg"),
        English(name:"cask",cht:"木桶", speak:"kæsk", picture:"cask.jpg"),
        English(name:"cat",cht:"貓", speak:"kæt", picture:"cat.jpg"),
        English(name:"cattle",cht:"牛", speak:"ˋkæt!", picture:"cattle.jpg"),
        English(name:"cavalryman",cht:"騎兵", speak:"ˋkæv!rɪmən", picture:"cavalryman.jpg"),
        English(name:"cave",cht:"山洞", speak:"kev", picture:"cave.jpg"),
        English(name:"cayenne",cht:"辣椒", speak:"ˋkeɛn", picture:"cayenne.jpg"),
        English(name:"censer",cht:"香爐", speak:"ˋsɛnsɚ", picture:"censer.jpg"),
        English(name:"centipede",cht:"蜈蚣", speak:"ˋsɛntə͵pid", picture:"centipede.jpg"),
        English(name:"cerebrum",cht:"大腦", speak:"ˋsɛrəbrəm", picture:"cerebrum.jpg"),
        English(name:"champion",cht:"冠軍", speak:"ˋtʃæmpɪən", picture:"champion.png"),
        English(name:"charmer",cht:"巫師", speak:"ˋtʃɑrmɚ", picture:"charmer.jpg"),
        English(name:"chattel",cht:"奴隸", speak:"ˋtʃæt!", picture:"chattel.jpg"),
        English(name:"check",cht:"支票", speak:"tʃɛk", picture:"check.jpg"),
        English(name:"clothes",cht:"衣服", speak:"kloz", picture:"clothes.jpg"),
        English(name:"Dad",cht:"爸爸", speak:"dad", picture:"Dad.jpg"),
        English(name:"Dance",cht:"跳舞", speak:"dans", picture:"Dance.jpg"),
        English(name:"Danger",cht:"危險", speak:"ˈdānjər", picture:"Danger.jpg"),
        English(name:"Dark",cht:"黑暗的", speak:"därk", picture:"Dark.jpg"),
        English(name:"Daughter",cht:"女兒", speak:"ˈdôdər", picture:"Daughter.jpg"),
        English(name:"Day",cht:"一天", speak:"de", picture:"Day.jpg"),
        English(name:"Dead",cht:"死的", speak:"ded", picture:"Dead.jpg"),
        English(name:"Decline",cht:"婉拒", speak:"dəˈklīn", picture:"Decline.jpg"),
        English(name:"Decompose",cht:"分解", speak:"ˌdēkəmˈpōz", picture:"Decompose.jpg"),
        English(name:"Decrease",cht:"減少", speak:"dəˈkrēs", picture:"Decrease.jpg"),
        English(name:"Deep",cht:"深的", speak:"dēp", picture:"Deep.jpg"),
        English(name:"Deer",cht:"鹿", speak:"dir", picture:"Deer.jpg"),
        English(name:"Delete",cht:"刪除", speak:"dəˈlēt", picture:"Delete.jpg"),
        English(name:"Deliver",cht:"傳送", speak:"dəˈlivər", picture:"Deliver.jpg"),
        English(name:"Demand",cht:"要求", speak:"dəˈmand", picture:"Demand.jpg"),
        English(name:"Design",cht:"設計", speak:"dəˈzīn", picture:"Design.jpg"),
        English(name:"Diary",cht:"日記", speak:"ˈdī(ərē", picture:"Diary.jpg"),
        English(name:"Different",cht:"不同的", speak:"ˈdif(ərənt", picture:"Different.jpg"),
        English(name:"Director",cht:"導演", speak:"diˈrektər", picture:"Director.jpg"),
        English(name:"Discuss",cht:"討論", speak:"dəˈskəs", picture:"Discuss.jpg"),
        English(name:"Dish",cht:"盤子", speak:"diSH", picture:"Dish.jpg"),
        English(name:"Doctor",cht:"醫生", speak:"ˈdäktər", picture:"Doctor.jpg"),
        English(name:"Dog",cht:"狗", speak:"dôɡ", picture:"Dog.jpg"),
        English(name:"Double",cht:"兩倍的", speak:"ˈdəb(əl", picture:"Double.jpg"),
        English(name:"Doubt",cht:"懷疑", speak:"dout", picture:"Doubt.jpg"),
        English(name:"Down",cht:"下降", speak:"doun", picture:"Down.jpg"),
        English(name:"Draw",cht:"畫", speak:"drô", picture:"Draw.jpg"),
        English(name:"Dream",cht:"夢想", speak:"drēm", picture:"Dream.jpg"),
        English(name:"Dress",cht:"裙子", speak:"dres", picture:"Dress.jpg"),
        English(name:"Dry",cht:"乾的", speak:"drī", picture:"Dry.jpg"),
        English(name:"eagle",cht:"老鷹", speak:"ˋig!", picture:"eagle.jpg"),
        English(name:"ear",cht:"耳朵", speak:"ir", picture:"ear.jpg"),
        English(name:"eardrum",cht:"耳膜", speak:"ˋɪr͵drʌm", picture:"eardrum.jpg"),
        English(name:"earlobe",cht:"耳垂", speak:"ˋɪr͵lob", picture:"earlobe.jpg"),
        English(name:"earmuffs",cht:"耳罩", speak:"ˋɪr͵mʌf", picture:"earmuffs.jpg"),
        English(name:"earnings",cht:"薪水", speak:"ˋɝnɪŋz", picture:"earnings.jpg"),
        English(name:"earphone",cht:"耳機", speak:"ˋɪr͵fon", picture:"earphone.jpg"),
        English(name:"earpiece",cht:"電話聽筒", speak:"ɪr͵pis", picture:"earpiece.jpg"),
        English(name:"earplug",cht:"耳塞", speak:"ˋɪr͵plʌg", picture:"earplug.jpg"),
        English(name:"earth",cht:"地球", speak:"ərTH", picture:"earth.jpg"),
        English(name:"earthworm",cht:"蚯蚓", speak:"ˋɝθ͵wɝm", picture:"earthworm.jpg"),
        English(name:"eat",cht:"吃", speak:"ēt", picture:"eat.jpg"),
        English(name:"economy",cht:"經濟", speak:"əˈkänəmē", picture:"economy.jpg"),
        English(name:"eel",cht:"鰻魚", speak:"il", picture:"eel.jpg"),
        English(name:"eight",cht:"八", speak:"et", picture:"eight.jpg"),
        English(name:"elbow",cht:"手肘", speak:"ˋɛlbo", picture:"elbow.jpg"),
        English(name:"electronic",cht:"電子的", speak:"ɪlɛkˋtrɑnɪk", picture:"electronic.jpg"),
        English(name:"elementary",cht:"小學", speak:"eləˈment(ərē", picture:"elementary.jpg"),
        English(name:"e-mail",cht:"電子郵件", speak:"ˈēmā", picture:"e-mail.jpg"),
        English(name:"embassy",cht:"大使館", speak:"ˋɛmbəsɪ", picture:"embassy.jpg"),
        English(name:"emperor",cht:"皇帝", speak:"ˋɛmpərɚ", picture:"emperor.jpg"),
        English(name:"encyclopedia",cht:"百科全書", speak:"ɪn͵saɪkləˋpidɪə", picture:"encyclopedia.jpg"),
        English(name:"engineer",cht:"工程師", speak:"ˌenjəˈnir", picture:"engineer.jpg"),
        English(name:"envelope",cht:"信封", speak:"ənˈveləp", picture:"envelope.jpg"),
        English(name:"equalize",cht:"均衡", speak:"ˋikwəl͵aɪz", picture:"equalize.jpg"),
        English(name:"eraser",cht:"橡皮擦", speak:"ɪˋresɚ", picture:"eraser.jpg"),
        English(name:"europe",cht:"歐洲", speak:"ˋjʊrəp", picture:"europe.jpg"),
        English(name:"exciting",cht:"興奮", speak:"ikˈsīdiNG", picture:"exciting.jpg"),
        English(name:"eye",cht:"眼睛", speak:"ī", picture:"eye.jpg"),
        English(name:"eyelid",cht:"眼皮", speak:"ˋaɪ͵lɪd", picture:"eyelid.jpg"),
        English(name:"face",cht:"臉", speak:"fes", picture:"face.png"),
        English(name:"factory",cht:"工廠", speak:"ˋfæməlɪ", picture:"factory.jpg"),
        English(name:"family",cht:"家庭", speak:"fæn", picture:"family.png"),
        English(name:"fan",cht:"扇子", speak:"fɑr", picture:"fan.png"),
        English(name:"far",cht:"遠的", speak:"fɑrm", picture:"far.png"),
        English(name:"farm",cht:"農場", speak:"ˋfɑrmɚ", picture:"farm.png"),
        English(name:"farmer",cht:"農夫", speak:"fæst", picture:"farmer.png"),
        English(name:"fast",cht:"快的", speak:"fæt", picture:"fast.png"),
        English(name:"fat",cht:"胖的", speak:"ˋfɑðɚ", picture:"fat.png"),
        English(name:"father",cht:"父親", speak:"ˋfæktərɪ", picture:"father.png"),
        English(name:"fifteen",cht:"十五", speak:"ˋfɪfˋtin", picture:"fifteen.jpg"),
        English(name:"fight",cht:"戰鬥", speak:"faɪt", picture:"fight.jpg"),
        English(name:"fireplace",cht:"壁爐", speak:"ˋfaɪr͵ples", picture:"fireplace.jpg"),
        English(name:"firework",cht:"煙火", speak:"ˋfaɪr͵wɝk", picture:"firework.jpg"),
        English(name:"flame",cht:"火焰", speak:"flem", picture:"flame.jpg"),
        English(name:"flea",cht:"跳蚤", speak:"fli", picture:"flea.jpg"),
        English(name:"flood",cht:"水災", speak:"flʌd", picture:"flood.jpg"),
        English(name:"flower",cht:"花", speak:"ˋflaʊɚ", picture:"flower.jpg"),
        English(name:"flute",cht:"橫笛", speak:"flut", picture:"flute.jpg"),
        English(name:"fly",cht:"蒼蠅", speak:"flaɪ", picture:"fly.jpg"),
        English(name:"fod",cht:"食物", speak:"fud", picture:"food.jpg"),
        English(name:"foot",cht:"腳", speak:"fʊt", picture:"foot.jpg"),
        English(name:"forge",cht:"冶煉場", speak:"fɔrdʒ", picture:"forge.jpg"),
        English(name:"fork",cht:"叉子", speak:"fɔrk", picture:"fork.jpg"),
        English(name:"fort",cht:"堡壘", speak:"fort", picture:"fort.jpg"),
        English(name:"fossil",cht:"化石", speak:"ˋfɑs", picture:"fossil.jpg"),
        English(name:"fox",cht:"狐狸", speak:"fɑks", picture:"fox.jpg"),
        English(name:"frog",cht:"青蛙", speak:"frɑg", picture:"frog.jpg"),
        English(name:"funeral",cht:"葬禮", speak:"ˋfjunərəl", picture:"funeral.jpg"),
        English(name:"funny",cht:"好笑的", speak:"ˋfʌnɪ", picture:"funny.jpg"),
        English(name:"gambler",cht:"賭徒", speak:"ˋgæmb!ɚ", picture:"gambler.jpg"),
        English(name:"game",cht:"遊戲", speak:"gem", picture:"game.jpg"),
        English(name:"gangster",cht:"匪徒", speak:"ˋgæŋstɚ", picture:"gangster.jpg"),
        English(name:"gaol",cht:"監獄", speak:"ˋdʒel", picture:"gaol.jpg"),
        English(name:"gap",cht:"缺口", speak:"gæp", picture:"gap.jpg"),
        English(name:"garbage",cht:"垃圾", speak:"ˋgɑrbɪdʒ", picture:"garbage.jpg"),
        English(name:"garden",cht:"花園", speak:"ˋgɑrdn", picture:"garden.jpg"),
        English(name:"gardener",cht:"園丁", speak:"ˋgɑrdənɚ", picture:"gardener.jpg"),
        English(name:"garlic",cht:"大蒜", speak:"ˋgɑrlɪk", picture:"garlic.jpg"),
        English(name:"garnet",cht:"石榴石", speak:"ˋgɑrnɪt", picture:"garnet.jpg"),
        English(name:"gasoline",cht:"汽油", speak:"ˋgæsə͵lin", picture:"gasoline.jpg"),
        English(name:"gate",cht:"大門", speak:"get", picture:"gate.jpg"),
        English(name:"gauze",cht:"薄紗", speak:"gɔz", picture:"gauze.jpg"),
        English(name:"gaze",cht:"注視", speak:"gez", picture:"gaze.jpg"),
        English(name:"gender",cht:"性別", speak:"ˋdʒɛndɚ", picture:"gender.jpg"),
        English(name:"germ",cht:"細菌", speak:"dʒɝm", picture:"germ.jpg"),
        English(name:"ghost",cht:"鬼魂", speak:"gost", picture:"ghost.png"),
        English(name:"giant",cht:"巨人", speak:"ˋdʒaɪənt", picture:"giant.jpg"),
        English(name:"gift",cht:"禮物", speak:"gɪft", picture:"gift.jpg"),
        English(name:"ginger",cht:"生薑", speak:"ˋdʒɪndʒɚ", picture:"ginger.jpg"),
        English(name:"girl",cht:"女孩", speak:"gɝl", picture:"girl.jpg"),
        English(name:"glass",cht:"玻璃", speak:"glæs", picture:"glass.jpg"),
        English(name:"glen",cht:"峽谷", speak:"glɛn", picture:"glen.jpg"),
        English(name:"global",cht:"球狀的", speak:"ˋglob!", picture:"global.jpg"),
        English(name:"glove",cht:"手套", speak:"glʌv", picture:"glove.jpg"),
        English(name:"goat",cht:"山羊", speak:"got", picture:"goat.jpg"),
        English(name:"god",cht:"上帝", speak:"gɑd", picture:"god.jpg"),
        English(name:"goods",cht:"商品", speak:"gʊdz", picture:"goods.jpg"),
        English(name:"goose",cht:"鵝", speak:"gus", picture:"goose.jpg"),
        English(name:"gorilla",cht:"大猩猩", speak:"gəˋrɪlə", picture:"gorilla.jpg"),
        English(name:"hack",cht:"老馬", speak:"hak", picture:"hack.jpg"),
        English(name:"hamburger",cht:"漢堡", speak:"ˈhamˌbərgər", picture:"hamburger.jpg"),
        English(name:"hammer",cht:"榔頭", speak:"hamər", picture:"hammer.jpg"),
        English(name:"hand",cht:"手", speak:"hand", picture:"hand.jpg"),
        English(name:"hand job",cht:"打手槍", speak:"handjäb", picture:"hand job.jpg"),
        English(name:"handle",cht:"處理", speak:"ˈhandl", picture:"handle.jpg"),
        English(name:"handsome",cht:"英俊的", speak:"ˈhansəm", picture:"handsome.jpg"),
        English(name:"hang",cht:"把...掛起", speak:"han", picture:"hang.jpg"),
        English(name:"happy",cht:"快樂", speak:"ˈhapē", picture:"happy.jpg"),
        English(name:"hard",cht:"難", speak:"hard", picture:"hard.jpg"),
        English(name:"hat",cht:"帽子", speak:"hat", picture:"hat.jpg"),
        English(name:"hate",cht:"討厭", speak:"hāt", picture:"hate.jpg"),
        English(name:"hen",cht:"母雞", speak:"hen]", picture:"hen.jpg"),
        English(name:"hide",cht:"隱藏", speak:"hīd", picture:"hide.jpg"),
        English(name:"highlight",cht:"使顯著", speak:"ˈhīlīt", picture:"highlight.jpg"),
        English(name:"hit",cht:"打", speak:"hit", picture:"hit.jpg"),
        English(name:"hold",cht:"握著", speak:"hold", picture:"hold.jpg"),
        English(name:"honor",cht:"榮譽", speak:"ˈänər", picture:"honor.jpg"),
        English(name:"hook",cht:"掛鉤", speak:"ho͝ok", picture:"hook.jpg"),
        English(name:"hooker",cht:"妓女", speak:"ho͝okər", picture:"hooker.jpg"),
        English(name:"hooligan",cht:"小流氓", speak:"ˈho͞oləgin", picture:"hooligan.jpg"),
        English(name:"hope",cht:"希望", speak:"hōp", picture:"hope.jpg"),
        English(name:"horse",cht:"馬", speak:"hôrs", picture:"horse.jpg"),
        English(name:"hospital",cht:"醫院", speak:"ˈhäˌspitl", picture:"hospital.jpg"),
        English(name:"host",cht:"主人", speak:"host", picture:"host.jpg"),
        English(name:"hot",cht:"熱", speak:"hät", picture:"hot.jpg"),
        English(name:"hotel",cht:"旅館", speak:"hōˈtel", picture:"hotel.jpg"),
        English(name:"house",cht:"房子", speak:"hous", picture:"house.jpg"),
        English(name:"humble",cht:"謙虛", speak:"ˈhəmbəl", picture:"humble.jpg"),
        English(name:"ice",cht:"冰", speak:"īs", picture:"ice.jpg"),
        English(name:"ice cream",cht:"冰淇淋", speak:"ˌaɪs ˈkrim", picture:"ice cream.jpg"),
        English(name:"icon",cht:"圖標", speak:"īˌkän", picture:"icon.png"),
        English(name:"idea",cht:"想法", speak:"īdz", picture:"idea.jpg"),
        English(name:"ideal",cht:"理想;志向", speak:"īˈdē(əl", picture:"ideal.png"),
        English(name:"ident",cht:"識別", speak:"ˈīdent", picture:"ident.jpg"),
        English(name:"idol",cht:"偶像", speak:"ˈīdl", picture:"idol.jpg"),
        English(name:"illegal",cht:"非法的", speak:"i(lˈlēɡəl", picture:"illegal.jpg"),
        English(name:"illness",cht:"疾病", speak:"ˈilnəs", picture:"illness.jpg"),
        English(name:"important",cht:"重要的", speak:"imˈpôrtnt", picture:"important.jpg"),
        English(name:"incident",cht:"事件", speak:"ˈinsəd(ənt", picture:"incident.png"),
        English(name:"income",cht:"收入", speak:"ˈinˌkəm", picture:"income.jpg"),
        English(name:"increase",cht:"增加", speak:"inˈkrēs", picture:"increase.jpg"),
        English(name:"independent",cht:"獨立的", speak:"ˌindəˈpendənt", picture:"independent.jpg"),
        English(name:"indicate",cht:"指示", speak:"ˈindəˌkāt", picture:"indicate.jpg"),
        English(name:"industry",cht:"工業;行業", speak:"ˈindəstrē", picture:"industry.jpg"),
        English(name:"information",cht:"資訊;訊息", speak:"ˌinfərˈmāSH(ən", picture:"information.jpg"),
        English(name:"injure",cht:"傷害", speak:"ˈinjər", picture:"injure.jpg"),
        English(name:"insect",cht:"昆蟲", speak:"ˈinˌsekt", picture:"insect.jpg"),
        English(name:"intelligence",cht:"智力", speak:"inˈteləjəns", picture:"intelligence.jpg"),
        English(name:"international",cht:"國際的", speak:"ˌin(tərˈnaSH(ən(əl", picture:"international.jpg"),
        English(name:"internet",cht:"網際網路", speak:"ˈin(tərˌnet", picture:"internet.jpg"),
        English(name:"invade",cht:"入侵", speak:"inˈvād", picture:"invade.jpg"),
        English(name:"invent",cht:"發明", speak:"inˈvent", picture:"invent.jpg"),
        English(name:"invest",cht:"投資", speak:"inˈvest", picture:"invest.jpg"),
        English(name:"investigate",cht:"調查", speak:"inˈvestəˌɡāt", picture:"investigate.jpg"),
        English(name:"invite",cht:"邀請", speak:"inˈvīt", picture:"invite.png"),
        English(name:"iron",cht:"鐵", speak:"ī(ərn", picture:"iron.jpg"),
        English(name:"island",cht:"島嶼", speak:"ˈīlənd", picture:"island.jpg"),
        English(name:"issue",cht:"議題", speak:"ˈiSHo͞o", picture:"issue.jpg"),
        English(name:"jacal",cht:"小茅屋", speak:"hɑˋkɑl", picture:"jacal.jpg"),
        English(name:"jacana",cht:"水雉", speak:"͵ʒɑsəˋnɑ", picture:"jacana.jpg"),
        English(name:"jackal",cht:"豺狼", speak:"ˋdʒækɔl", picture:"jackal.jpg"),
        English(name:"jackboot",cht:"長筒靴", speak:"ˋdʒæk͵but", picture:"jackboot.jpg"),
        English(name:"jacket",cht:"夾克", speak:"ˋdʒækIt", picture:"jacket.jpg"),
        English(name:"jackfruit",cht:"菠蘿蜜", speak:"ˋdʒæk͵frut", picture:"jackfruit.jpg"),
        English(name:"jackknife",cht:"摺疊刀", speak:"ˋdʒæk͵naɪf", picture:"jackknife.jpg"),
        English(name:"jackshaft",cht:"起重機", speak:"ˋdʒæk͵ʃæft", picture:"jackshaft.jpg"),
        English(name:"jail",cht:"監獄", speak:"dʒel", picture:"jail.jpg"),
        English(name:"jam",cht:"果醬", speak:"dʒæm", picture:"jam.jpg"),
        English(name:"jamb",cht:"門邊框柱", speak:"dʒæm", picture:"jamb.jpg"),
        English(name:"jazz",cht:"爵士樂", speak:"dʒæz", picture:"jazz.jpg"),
        English(name:"jeans",cht:"牛仔褲", speak:"dʒinz", picture:"jeans.jpg"),
        English(name:"jelly",cht:"果凍", speak:"ˋdʒɛlɪ", picture:"jelly.jpg"),
        English(name:"jet",cht:"噴射機", speak:"dʒZt", picture:"jet.jpg"),
        English(name:"jewelry",cht:"珠寶", speak:"ˋdʒuәlrI", picture:"jewelry.jpg"),
        English(name:"job",cht:"工作", speak:"dʒɑb", picture:"job.jpg"),
        English(name:"jog",cht:"慢跑", speak:"dʒɑg", picture:"jog.jpg"),
        English(name:"joke",cht:"笑話", speak:"dʒok", picture:"joke.jpg"),
        English(name:"journal",cht:"日誌", speak:"ˋdʒFnL", picture:"journal.jpg"),
        English(name:"journey",cht:"旅行", speak:"ˋdʒFnI", picture:"journey.jpg"),
        English(name:"joy",cht:"歡樂", speak:"dʒɔɪ", picture:"joy.jpg"),
        English(name:"judge",cht:"法官", speak:"dʒʌdʒ", picture:"judge.jpg"),
        English(name:"jug",cht:"水壺", speak:"dʒʌg", picture:"jug.jpg"),
        English(name:"juice",cht:"果汁", speak:"dʒus", picture:"juice.jpg"),
        English(name:"jump",cht:"跳", speak:"dʒʌmp", picture:"jump.jpg"),
        English(name:"jungle",cht:"叢林", speak:"ˋdʒʌŋgL", picture:"jungle.jpg"),
        English(name:"junk",cht:"垃圾", speak:"dʒʌŋk", picture:"junk.jpg"),
        English(name:"jury",cht:"陪審團", speak:"ˈdʒʊrɪ", picture:"jury.jpg"),
        English(name:"justice",cht:"公平", speak:"ˋdʒʌstIs", picture:"justice.jpg"),
        English(name:"Kaiser",cht:"皇帝", speak:"ˋkaɪzɚ", picture:"Kaiser.jpg"),
        English(name:"kaleidoscope",cht:"萬花筒", speak:"kəˋlaɪdə͵skop", picture:"kaleidoscope.jpg"),
        English(name:"kangaroo",cht:"袋鼠", speak:"͵kæŋgəˋru", picture:"kangaroo.jpg"),
        English(name:"karate",cht:"空手道", speak:"kəˋrɑtɪ", picture:"karate.jpg"),
        English(name:"keel",cht:"平底船", speak:"kil", picture:"keel.jpg"),
        English(name:"kennel",cht:"狗窩", speak:"ˋkɛn!", picture:"kennel.jpg"),
        English(name:"kerchief",cht:"手帕", speak:"ˋkɝtʃɪf", picture:"kerchief.jpg"),
        English(name:"kettle",cht:"水壺", speak:"ˋkɛt!", picture:"kettle.jpg"),
        English(name:"key",cht:"鑰匙", speak:"ki", picture:"key.png"),
        English(name:"keyboard",cht:"鍵盤", speak:"ˋki͵bord", picture:"keyboard.jpg"),
        English(name:"keypad",cht:"小鍵盤", speak:"ˋki͵pæd", picture:"keypad.jpg"),
        English(name:"kid",cht:"少年", speak:"kɪd", picture:"kid.jpg"),
        English(name:"kill",cht:"殺", speak:"kɪl", picture:"kill.jpg"),
        English(name:"killer",cht:"殺手", speak:"ˋkɪlɚ", picture:"killer.jpg"),
        English(name:"kindergarten",cht:"幼稚園", speak:"ˋkɪndɚ͵gɑrtn", picture:"kindergarten.jpg"),
        English(name:"king",cht:"國王", speak:"kɪŋ", picture:"king.png"),
        English(name:"kip",cht:"旅店", speak:"kɪp", picture:"kip.jpg"),
        English(name:"kiss",cht:"吻", speak:"kɪs", picture:"kiss.jpg"),
        English(name:"kitchen",cht:"廚房", speak:"ˋkɪtʃɪn", picture:"kitchen.jpg"),
        English(name:"kite",cht:"風箏", speak:"kaɪt", picture:"kite.jpg"),
        English(name:"kitten",cht:"小貓", speak:"ˋkɪtn", picture:"kitten.jpg"),
        English(name:"knead",cht:"揉", speak:"nid", picture:"knead.jpg"),
        English(name:"knee",cht:"膝蓋", speak:"ni", picture:"knee.jpg"),
        English(name:"kneel",cht:"跪下", speak:"nil", picture:"kneel.png"),
        English(name:"knife",cht:"刀子", speak:"naɪf", picture:"knife.jpg"),
        English(name:"knight",cht:"騎士", speak:"naɪt", picture:"knight.jpg"),
        English(name:"knob",cht:"門把", speak:"nɑb", picture:"knob.jpg"),
        English(name:"knock",cht:"敲", speak:"nɑk", picture:"knock.jpg"),
        English(name:"knot",cht:"繩結", speak:"nɑt", picture:"knot.jpg"),
        English(name:"ksyak",cht:"獨木舟", speak:"ˈkaɪæk", picture:"ksyak.jpg"),
        English(name:"lace",cht:"緞帶", speak:"lās", picture:"lacep.jpg"),
        English(name:"lack",cht:"缺乏", speak:"lak", picture:"lackp.jpg"),
        English(name:"lackadaisical",cht:"不客氣", speak:"ˌlakəˈdāzək(əl", picture:"lackadaisicalp.png"),
        English(name:"lacrimal",cht:"眼淚", speak:"ˈlakrəməl", picture:"lacrimalp.png"),
        English(name:"lactic",cht:"乳酸", speak:"ˈlaktik", picture:"lacticp.jpg"),
        English(name:"lacuna",cht:"空白", speak:"ləˈk(yo͞onə", picture:"lacunap.png"),
        English(name:"laden",cht:"負擔", speak:"lādn", picture:"ladenp.png"),
        English(name:"lady",cht:"女士", speak:"ˈlādē", picture:"ladyp.jpg"),
        English(name:"laic",cht:"凡人", speak:"ˈlāik", picture:"laicp.png"),
        English(name:"lake",cht:"湖", speak:"lāk", picture:"lakep.jpg"),
        English(name:"lamb",cht:"羔羊", speak:"lam", picture:"lambp.jpg"),
        English(name:"lamp",cht:"檯燈", speak:"lamp", picture:"lampp.jpg"),
        English(name:"land",cht:"陸地", speak:"land", picture:"landp.jpg"),
        English(name:"language",cht:"語言", speak:"ˈlaNGɡwij", picture:"languagep.png"),
        English(name:"laugh",cht:"笑", speak:"laf", picture:"laughp.png"),
        English(name:"lawyer",cht:"律師", speak:"ˈloiər", picture:"lawyerp.jpg"),
        English(name:"lazy",cht:"懶惰", speak:"ˈlāzē", picture:"lazyp.png"),
        English(name:"leave",cht:"離開", speak:"lēv", picture:"leavep.jpg"),
        English(name:"leg",cht:"腳", speak:"leɡ", picture:"legp.png"),
        English(name:"lemon",cht:"檸檬", speak:"ˈlemən", picture:"lemonp.jpg"),
        English(name:"lesson",cht:"課堂", speak:"ˈles(ən", picture:"lessonp.png"),
        English(name:"lettuce",cht:"萵苣", speak:"ˈledəs", picture:"lettucep.jpg"),
        English(name:"lid",cht:"蓋子", speak:"lid", picture:"lidp.jpg"),
        English(name:"lie",cht:"謊言", speak:"lī", picture:"liep.jpg"),
        English(name:"life",cht:"生命", speak:"līf", picture:"lifep.jpg"),
        English(name:"limit",cht:"極限", speak:"ˈlimit", picture:"limitp.png"),
        English(name:"location",cht:"位置", speak:"lōˈkāSH(ən", picture:"locationp.png"),
        English(name:"lolly",cht:"棒棒糖", speak:"ˈlälē", picture:"lollyp.png"),
        English(name:"lose",cht:"喪失", speak:"lo͞oz", picture:"losep.jpg"),
        English(name:"lucky",cht:"幸運", speak:"ˈləkē", picture:"luckyp.jpg"),
        English(name:"macadam",cht:"碎石路", speak:"məˋkædəm", picture:"macadam.jpg"),
        English(name:"macaron",cht:"蛋白杏仁餅乾", speak:"͵mækəˋrun", picture:"macaroon.jpg"),
        English(name:"macaroni",cht:"通心麵", speak:"͵mækəˋronɪ", picture:"macaroni.jpg"),
        English(name:"mace",cht:"權杖", speak:"mes", picture:"mace.jpg"),
        English(name:"machete",cht:"大砍刀", speak:"mɑˋtʃete", picture:"machete.jpg"),
        English(name:"mackerel",cht:"鯖魚", speak:"ˋmækərəl", picture:"mackerel.jpg"),
        English(name:"mackintosh",cht:"風衣", speak:"ˈmækɪntɒʃ", picture:"mackintosh.jpg"),
        English(name:"Madonna",cht:"聖母瑪利亞", speak:"məˋdɑnə", picture:"Madonna.jpg"),
        English(name:"maelstorm",cht:"混亂", speak:"ˈmeɪlstrəm", picture:"maelstorm.jpg"),
        English(name:"magazine",cht:"雜誌", speak:"͵mægəˋzin", picture:"magazine.jpg"),
        English(name:"magic",cht:"魔法", speak:"ˋmædʒɪk", picture:"magic.jpg"),
        English(name:"magnifier",cht:"放大鏡", speak:"ˋmægnə͵faɪɚ", picture:"magnifier.png"),
        English(name:"mail",cht:"郵件", speak:"mel", picture:"mail.png"),
        English(name:"mailbox",cht:"郵筒", speak:"ˋmel͵bɑks", picture:"mailbox.jpg"),
        English(name:"mailman",cht:"郵差", speak:"ˋmel͵mæn", picture:"mailman.jpg"),
        English(name:"mainmast",cht:"主桅", speak:"ˋmen͵mæst", picture:"mainmast.jpg"),
        English(name:"maize",cht:"玉蜀黍", speak:"mez", picture:"maize.jpg"),
        English(name:"male",cht:"男子", speak:"mel", picture:"male.png"),
        English(name:"malefactor",cht:"罪人", speak:"ˋmælə͵fæktɚ", picture:"malefactor.jpg"),
        English(name:"malt",cht:"啤酒", speak:"mɔlt", picture:"malt.jpg"),
        English(name:"mango",cht:"芒果", speak:"ˋmæŋgo", picture:"mango.jpg"),
        English(name:"manor",cht:"莊園", speak:"ˋmænɚ", picture:"manor.jpg"),
        English(name:"manservant",cht:"女僕", speak:"ˋmæn͵sɝvənt", picture:"manservant.png"),
        English(name:"manure",cht:"肥料", speak:"məˋnjʊr", picture:"manure.jpg"),
        English(name:"maple",cht:"楓樹", speak:"ˋmep!", picture:"maple.jpg"),
        English(name:"marble",cht:"大理石", speak:"ˋmɑrb!", picture:"marble.jpg"),
        English(name:"mariner",cht:"水手", speak:"ˋmærənɚ", picture:"mariner.png"),
        English(name:"marionette",cht:"牽線木偶", speak:"͵mærɪəˋnɛt", picture:"marionette.jpg"),
        English(name:"marish",cht:"沼澤", speak:"ˋmærɪʃ", picture:"marish.jpg"),
        English(name:"mom",cht:"母親", speak:"mɑm", picture:"mom.jpg"),
        English(name:"narrow",cht:"狹隘的;狹窄的", speak:"ˋnæro", picture:"narrow.jpg"),
        English(name:"nearsighted",cht:"近視的", speak:"ˋnɪrˋsaɪtɪd", picture:"nearsighted.jpg"),
        English(name:"neat",cht:"整潔的", speak:"nit", picture:"neat.jpg"),
        English(name:"necessary",cht:"必要的", speak:"ˋnɛsə͵sɛrɪ", picture:"necessary.png"),
        English(name:"needy",cht:"貧窮的", speak:"ˋnidɪ", picture:"needy.jpg"),
        English(name:"neglect",cht:"忽視", speak:"nɪgˋlɛkt", picture:"neglect.jpg"),
        English(name:"negligence",cht:"粗心", speak:"ˋnɛglɪdʒəns", picture:"negligence.jpg"),
        English(name:"negligible",cht:"微不足道的", speak:"ˋnɛglɪdʒəb!", picture:"negligible.png"),
        English(name:"negotiable",cht:"可協商的", speak:"nɪˋgoʃɪəb!", picture:"negotiable.jpg"),
        English(name:"neighborly",cht:"親切的", speak:"ˋnebɚlɪ", picture:"neighborly.jpg"),
        English(name:"nephew",cht:"姪兒", speak:"ˋnɛfju", picture:"nephew.jpg"),
        English(name:"nerve",cht:"神經", speak:"nɝv", picture:"nerve.jpg"),
        English(name:"nevertheless",cht:"仍然", speak:"͵nɛvɚðəˋlɛs", picture:"nevertheless.png"),
        English(name:"newlywed",cht:"新結婚的人", speak:"ˋnjulɪ͵wɛd", picture:"newlywed.jpg"),
        English(name:"nickel",cht:"鎳", speak:"ˋnɪk!", picture:"nickel.jpg"),
        English(name:"nifty",cht:"俏皮的", speak:"ˋnɪftɪ", picture:"nifty.jpg"),
        English(name:"nimble",cht:"靈活的", speak:"ˋnɪmb!", picture:"nimble.jpg"),
        English(name:"nine",cht:"九", speak:"naɪn", picture:"nine.jpg"),
        English(name:"nitrogen",cht:"氮", speak:"ˋnaɪtrədʒən", picture:"nitrogen.png"),
        English(name:"noble",cht:"高貴的", speak:"ˋnob!", picture:"noble.png"),
        English(name:"nocturne",cht:"夜曲", speak:"ˋnɑktɝn", picture:"nocturne.jpg"),
        English(name:"nominal",cht:"名義上的", speak:"ˋnɑmən!", picture:"nominal.jpg"),
        English(name:"nominate",cht:"題名", speak:"ˋnɑmə͵net", picture:"nominate.jpg"),
        English(name:"notable",cht:"顯著的", speak:"ˋnotəb!", picture:"notable.jpg"),
        English(name:"notorious",cht:"惡名昭彰的", speak:"noˋtorɪəs", picture:"notorious.png"),
        English(name:"novelty",cht:"新穎", speak:"ˋnɑv!tɪ", picture:"novelty.jpg"),
        English(name:"nucleus",cht:"（原子）核", speak:"ˋnjuklɪəs", picture:"nucleus.jpg"),
        English(name:"nuisance",cht:"麻煩事", speak:"ˋnjusns", picture:"nuisance.jpg"),
        English(name:"nursery",cht:"幼兒室", speak:"ˋnɝsərɪ", picture:"nursery.jpg"),
        English(name:"nutritionist",cht:"營養學家", speak:"njuˋtrɪʃənɪst", picture:"nutritionist.jpg"),
        English(name:"oak",cht:"橡樹", speak:"oʊk", picture:"oak.jpg"),
        English(name:"oar",cht:"槳", speak:"ɔr, or", picture:"oar.jpg"),
        English(name:"oasis",cht:"綠洲", speak:"oʊˈeɪsɪs", picture:"oasis.jpg"),
        English(name:"oath",cht:"宣誓", speak:"oʊθ", picture:"oath.jpg"),
        English(name:"obey",cht:"服從", speak:"oˈbe", picture:"obey.jpg"),
        English(name:"obituary",cht:"死亡公告", speak:"oʊˈbɪtʃueri", picture:"obituary.jpg"),
        English(name:"object",cht:"物件", speak:"ˈɑbdʒekt", picture:"object.jpg"),
        English(name:"obscure",cht:"不清楚的", speak:"əbˈskjʊr", picture:"obscure.jpg"),
        English(name:"observatory",cht:"觀測所", speak:"əbˈzɜrvətɔri", picture:"observatory.jpg"),
        English(name:"obstacle",cht:"障礙物", speak:"ˈɑbstəkl", picture:"obstacle.jpg"),
        English(name:"octopus",cht:"章魚", speak:"ˈɑktəpəs]", picture:"octopus.jpg"),
        English(name:"office",cht:"辦公室", speak:"ˈɔfɪs", picture:"office.jpg"),
        English(name:"officer",cht:"軍官,官吏,警官,警察", speak:"ˈɔfɪsə(r)", picture:"officer.jpg"),
        English(name:"onion",cht:"洋蔥", speak:"ˈʌnjən", picture:"onion.jpg"),
        English(name:"op",cht:"開刀手術", speak:"ɑp", picture:"op.jpg"),
        English(name:"opener",cht:"開瓶器,開罐器", speak:"ˈəʊpnə(r)", picture:"opener.jpg"),
        English(name:"opera",cht:"歌劇", speak:"ˈɑprə", picture:"opera.jpg"),
        English(name:"oppose",cht:"反對", speak:"əˈpoʊz", picture:"oppose.jpg"),
        English(name:"orchestra",cht:"管弦樂隊", speak:"ˈɔ:kɪstrə", picture:"orchestra.jpg"),
        English(name:"ore",cht:"礦石", speak:"ɔr", picture:"ore.jpg"),
        English(name:"organ",cht:"器官,風琴", speak:"ˈɔ:gən", picture:"organ.jpg"),
        English(name:"oscillator",cht:"震盪器", speak:"ˈɒsɪleɪtə(r)", picture:"oscillator.jpg"),
        English(name:"ostrich",cht:"鴕鳥", speak:"ˈɒstrɪtʃ", picture:"ostrich.jpg"),
        English(name:"outbreak",cht:"暴動", speak:"ˈaʊtbreɪk", picture:"outbreak.jpg"),
        English(name:"outskirts",cht:"郊區", speak:"ˈaʊtskɜ:ts", picture:"outskirts.jpg"),
        English(name:"oven",cht:"烤箱", speak:"ˈʌvn", picture:"oven.jpg"),
        English(name:"overlook",cht:"俯視", speak:"ˌəʊvəˈlʊk", picture:"overlook.jpg"),
        English(name:"overpass",cht:"天橋", speak:"ˈəʊvəpɑ:s", picture:"overpass.jpg"),
        English(name:"owl",cht:"貓頭鷹", speak:"aʊl", picture:"owl.jpg"),
        English(name:"ox",cht:"公牛", speak:"ɒks", picture:"ox.jpg"),
        English(name:"packman",cht:"小販", speak:"ˋpækmən", picture:"packman.jpg"),
        English(name:"paction",cht:"契約書", speak:"ˋpækʃən", picture:"paction.jpg"),
        English(name:"padre",cht:"神父", speak:"ˋpɑdrɪ", picture:"padre.jpg"),
        English(name:"palace",cht:"宮殿", speak:"ˋpælɪs", picture:"palace.jpg"),
        English(name:"pamphlet",cht:"小冊子", speak:"ˋpæmflɪt", picture:"pamphlet.jpg"),
        English(name:"pan",cht:"平底鍋", speak:"pæn", picture:"pan.jpg"),
        English(name:"pancake",cht:"鬆餅", speak:"ˋpæn͵kek", picture:"pancake.jpg"),
        English(name:"panda",cht:"貓熊", speak:"ˋpændə", picture:"panda.jpg"),
        English(name:"parakeet",cht:"鸚鵡", speak:"ˋpærə͵kit", picture:"parakeet.jpg"),
        English(name:"parasol",cht:"陽傘", speak:"ˋpærə͵sɔl", picture:"parasol.jpg"),
        English(name:"parcel",cht:"包裹", speak:"ˋpɑrs!", picture:"parcel.jpg"),
        English(name:"pard",cht:"豹", speak:"pɑrd", picture:"pard.jpg"),
        English(name:"partridge",cht:"鷓鴣", speak:"ˋpɑrtrɪdʒ", picture:"partridge.jpg"),
        English(name:"passenger",cht:"乘客", speak:"ˋpæsndʒɚ", picture:"passenger.png"),
        English(name:"pastry",cht:"糕點", speak:"ˋpestrɪ", picture:"pastry.jpg"),
        English(name:"pasture",cht:"牧場", speak:"ˋpæstʃɚ", picture:"pasture.jpg"),
        English(name:"patio",cht:"露臺", speak:"ˋpɑtɪ͵o", picture:"patio.jpg"),
        English(name:"pear",cht:"梨", speak:"pɛr", picture:"pear.jpg"),
        English(name:"pedal",cht:"踏板", speak:"ˋpɛd!", picture:"pedal.jpg"),
        English(name:"peddler",cht:"小販", speak:"ˋpɛdlɚ", picture:"peddler.jpg"),
        English(name:"pencil",cht:"鉛筆", speak:"ˋpɛns!", picture:"pencil.png"),
        English(name:"penguin",cht:"企鵝", speak:"ˋpɛngwɪn", picture:"penguin.jpg"),
        English(name:"peony",cht:"牡丹", speak:"ˋpiənɪ", picture:"peony.jpg"),
        English(name:"periwig",cht:"假髮", speak:"ˋpɛrə͵wɪg", picture:"periwig.jpg"),
        English(name:"permit",cht:"執照", speak:"pɚˋmɪt", picture:"permit.jpg"),
        English(name:"persimmon",cht:"柿子", speak:"pɚˋsɪmən", picture:"persimmon.jpg"),
        English(name:"pesticide",cht:"殺蟲劑", speak:"ˋpɛstɪ͵saɪd", picture:"pesticide.jpg"),
        English(name:"petal",cht:"花瓣", speak:"ˋpɛt!", picture:"petal.jpg"),
        English(name:"petrel",cht:"海燕", speak:"ˋpɛtrəl", picture:"petrel.jpg"),
        English(name:"petroleum",cht:"石油", speak:"pəˋtrolɪəm", picture:"petroleum.jpg"),
        English(name:"quack",cht:"庸醫", speak:"kwæk", picture:"quack.jpg"),
        English(name:"quadrangle",cht:"四邊形", speak:"ˋkwɑdræŋg!", picture:"quadrangle.png"),
        English(name:"quagmire",cht:"沼澤地", speak:"ˋkwæg͵maɪr", picture:"quagmire.jpg"),
        English(name:"quail",cht:"鵪鶉", speak:"kwel", picture:"quail.jpg"),
        English(name:"quaint",cht:"古色古香的", speak:"kweɪnt", picture:"quaint.jpg"),
        English(name:"quake",cht:"地震", speak:"kwek", picture:"quake.jpg"),
        English(name:"qualification",cht:"執照", speak:"͵kwɑləfəˋkeʃən", picture:"qualification.jpg"),
        English(name:"qualm",cht:"不安", speak:"kwɔm", picture:"qualm.jpg"),
        English(name:"quandary",cht:"困惑", speak:"ˋkwɑndərɪ", picture:"quandary.jpg"),
        English(name:"quarantine",cht:"隔離", speak:"ˋkwɔrən͵tin", picture:"quarantine.png"),
        English(name:"quarrel",cht:"爭吵", speak:"ˋkwɔrəl", picture:"quarrel.jpg"),
        English(name:"quarry",cht:"採石場", speak:"ˋkwɔrɪ", picture:"quarry.jpg"),
        English(name:"quarter",cht:"住所", speak:"ˋkwɔrtɚ", picture:"quarter.jpg"),
        English(name:"quartermaster",cht:"軍需官", speak:"ˋkwɔrtɚ͵mæstɚ", picture:"quartermaster.jpg"),
        English(name:"quartet",cht:"四重奏", speak:"kwɔrˋtɛt", picture:"quartet.png"),
        English(name:"quartz",cht:"石英", speak:"kwɔrts", picture:"quartz.jpg"),
        English(name:"quasar",cht:"類星體", speak:"ˋkwesɑr", picture:"quasar.jpg"),
        English(name:"quaver",cht:"震動", speak:"ˋkwevɚ", picture:"quaver.jpg"),
        English(name:"quay",cht:"碼頭", speak:"ki", picture:"quay.jpg"),
        English(name:"queen",cht:"女皇", speak:"ˋkwin", picture:"queen.png"),
        English(name:"quell",cht:"鎮壓", speak:"kwɛl", picture:"quell.jpg"),
        English(name:"quench",cht:"熄滅", speak:"kwɛntʃ", picture:"quench.jpg"),
        English(name:"quest",cht:"尋找", speak:"kwɛst", picture:"quest.jpg"),
        English(name:"questionable",cht:"可疑的", speak:"ˋkwɛstʃənəb!", picture:"questionable.jpg"),
        English(name:"questionnaire",cht:"問卷", speak:"͵kwɛstʃənˋɛr", picture:"questionnaire.jpg"),
        English(name:"queue",cht:"列隊", speak:"kju", picture:"queue.jpg"),
        English(name:"quicksand",cht:"流沙", speak:"ˋkwɪk͵sænd", picture:"quicksand.jpg"),
        English(name:"quicksilver",cht:"水銀", speak:"ˋkwɪk͵sɪlvɚ", picture:"quicksilver.jpg"),
        English(name:"quilt",cht:"被子", speak:"kwɪlt", picture:"quilt.jpg"),
        English(name:"quisling",cht:"內奸", speak:"ˋkwɪz!ɪŋ", picture:"quisling.jpg"),
        English(name:"rabbit",cht:"兔子", speak:"ˈrabət", picture:"rabbit.jpg"),
        English(name:"raccoon",cht:"浣熊", speak:"raˈko͞on", picture:"raccoon.jpg"),
        English(name:"racket",cht:"球拍", speak:"ˈrakət", picture:"racket.jpg"),
        English(name:"radish",cht:"小蘿蔔", speak:"ˈradiSH", picture:"radish.jpg"),
        English(name:"railway",cht:"鐵路", speak:"ˈrālˌwā", picture:"railway.jpg"),
        English(name:"rainbow",cht:"彩虹", speak:"ˈrānˌbō", picture:"rainbow.jpg"),
        English(name:"rainbow trout",cht:"虹鱒魚", speak:"ˈˌrānbō ˈtrout", picture:"rainbow trout.jpg"),
        English(name:"raisin",cht:"葡萄乾", speak:"ˈrāzən", picture:"raisin.jpg"),
        English(name:"ram",cht:"公羊", speak:"ram", picture:"ram.jpg"),
        English(name:"razorback",cht:"野豬", speak:"ˈrāzər", picture:"bak"),
        English(name:"red squirrel",cht:"紅松鼠", speak:"red ˈskwər(əl", picture:"red squirrel.jpg"),
        English(name:"redbird",cht:"紅雀", speak:"ˈredbə:d", picture:"redbird.jpg"),
        English(name:"redbreast",cht:"知更鳥", speak:"ˈredˌbrest", picture:"redbreast.jpg"),
        English(name:"redhead",cht:"紅頭鴨", speak:"ˈredˌhed", picture:"redhead.jpg"),
        English(name:"redwing",cht:"紅翼鶇", speak:"ˈredwiNG", picture:"redwing.jpg"),
        English(name:"refrigerator",cht:"冰箱", speak:"rəˈfrijəˌrādər", picture:"refrigerator.jpg"),
        English(name:"reindeer",cht:"馴鹿", speak:"ˈrānˌdi", picture:"reindeer.jpg"),
        English(name:"reservoir",cht:"水庫", speak:"ˈrezər", picture:"vwär"),
        English(name:"restaurant",cht:"餐廳", speak:"ˈrest(ərənt", picture:"restaurant.jpg"),
        English(name:"rhinoceros",cht:"犀牛", speak:"rīˈnäs(ərəs", picture:"rhinoceros.jpg"),
        English(name:"rice",cht:"米飯", speak:"rīs", picture:"rice.jpg"),
        English(name:"rick",cht:"乾草堆", speak:"rik", picture:"rick.jpg"),
        English(name:"ringdove",cht:"斑鳩", speak:"ˈriNGdəv", picture:"ringdove.jpg"),
        English(name:"rink",cht:"溜冰場", speak:"riNGk", picture:"rink.jpg"),
        English(name:"river",cht:"河", speak:"ˈrivər", picture:"river.jpg"),
        English(name:"robot",cht:"機器人", speak:"ˈrōbət", picture:"robot.jpg"),
        English(name:"roebuck",cht:"雄鹿", speak:"ˈrōˌbək", picture:"roebuck.jpg"),
        English(name:"rooster",cht:"公雞", speak:"ˈro͞ostər", picture:"rooster.jpg"),
        English(name:"rose",cht:"玫瑰花", speak:"rōz", picture:"rose.jpg"),
        English(name:"rosemary",cht:"迷迭香", speak:"ˈrōz", picture:"merē"),
        English(name:"saber",cht:"軍刀", speak:"ˋsebɚ", picture:"saber.jpg"),
        English(name:"sable",cht:"黑貂", speak:"ˋseb!", picture:"sable.jpg"),
        English(name:"sachem",cht:"酋長", speak:"ˋsetʃəm", picture:"sachem.jpg"),
        English(name:"sacrament",cht:"聖禮", speak:"ˋsækrəmənt", picture:"sacrament.jpg"),
        English(name:"saddle",cht:"馬鞍", speak:"ˋsæd!", picture:"saddle.jpg"),
        English(name:"sailboat",cht:"帆船", speak:"ˋsel͵bot", picture:"sailboat.jpg"),
        English(name:"sailor",cht:"水手", speak:"ˋselɚ", picture:"sailor.jpg"),
        English(name:"salacity",cht:"好色", speak:"səˋlæsətɪ", picture:"salacity.jpg"),
        English(name:"salad",cht:"沙拉", speak:"ˋsæləd", picture:"salad.jpg"),
        English(name:"salamander",cht:"火蜥蜴", speak:"ˋsælə͵mændɚ", picture:"salamander.jpg"),
        English(name:"salary",cht:"薪水", speak:"ˋsælərɪ", picture:"salary.jpg"),
        English(name:"sale",cht:"銷售", speak:"sel", picture:"sale.jpg"),
        English(name:"salt",cht:"鹽", speak:"sɔlt", picture:"salt.jpg"),
        English(name:"saltpeter",cht:"硝石", speak:"ˋsɔlt͵pitɚ", picture:"saltpeter.jpg"),
        English(name:"samisen",cht:"三弦琴", speak:"ˋsæmə͵sɛn", picture:"samisen.jpg"),
        English(name:"sanctuary",cht:"聖堂", speak:"ˋsæŋktʃʊ͵ɛrɪ", picture:"sanctuary.jpg"),
        English(name:"sand",cht:"沙", speak:"sænd", picture:"sand.jpg"),
        English(name:"sap",cht:"樹液", speak:"sæp", picture:"sap.jpg"),
        English(name:"sarcoma",cht:"肉瘤", speak:"sɑrˋkomə", picture:"sarcoma.jpg"),
        English(name:"sarcophagus",cht:"石棺", speak:"sɑrˋkɑfəgəs", picture:"sarcophagus.jpg"),
        English(name:"sardine",cht:"沙丁魚", speak:"sɑrˋdin", picture:"sardine.jpg"),
        English(name:"sargasso",cht:"馬尾藻", speak:"sɑrˋgæso", picture:"sargasso.jpg"),
        English(name:"satchel",cht:"書包", speak:"ˋsætʃəl", picture:"satchel.jpg"),
        English(name:"Saturn",cht:"土星", speak:"ˋsætɚn", picture:"Saturn.jpg"),
        English(name:"sausage",cht:"香腸", speak:"ˋsɔsɪdʒ", picture:"sausage.jpg"),
        English(name:"scabbard",cht:"槍套", speak:"ˋskæbɚd", picture:"scabbard.jpg"),
        English(name:"scale",cht:"天秤", speak:"skel", picture:"scale.jpg"),
        English(name:"scallop",cht:"扇貝", speak:"ˋskɑləp", picture:"scallop.jpg"),
        English(name:"slaver",cht:"盤子", speak:"ˋslevɚ", picture:"slaver.jpg"),
        English(name:"surf",cht:"海浪", speak:"sɝf", picture:"surf.jpg"),
        English(name:"Tablespoon",cht:"大湯匙", speak:"ˈtābəlˌspo͞on", picture:"tablespoon.jpg"),
        English(name:"Tablet",cht:"平板", speak:"ˈtablət", picture:"tablet.jpg"),
        English(name:"Tailor",cht:"裁縫師", speak:"ˈtālər", picture:"tailor.jpg"),
        English(name:"Tale",cht:"故事", speak:"tāl", picture:"tale.jpg"),
        English(name:"Tangerine",cht:"橘子", speak:"ˌtanjəˈrēn", picture:"tangerine.jpg"),
        English(name:"Tanker",cht:"油輪", speak:"ˈtaNGkər", picture:"tanker.jpg"),
        English(name:"Technician",cht:"技術人員", speak:"tekˈniSHən", picture:"technician.jpg"),
        English(name:"Teenager",cht:"青少年", speak:"ˈtēnˌājər", picture:"teenager.png"),
        English(name:"Telegram",cht:"電報", speak:"telegram", picture:"telegram.jpg"),
        English(name:"Telescope",cht:"望遠鏡", speak:"ˈteləˌskōp", picture:"telescope.jpg"),
        English(name:"Television",cht:"電視", speak:"ˈteləˌviZHən", picture:"television.jpg"),
        English(name:"Temperature",cht:"溫度", speak:"ˈtemp(ərəCHər", picture:"temperature.jpg"),
        English(name:"Throat",cht:"喉嚨", speak:"THrōt", picture:"throat.jpg"),
        English(name:"Thunderstorm",cht:"大雷雨", speak:"ˈTHəndərˌstôrm", picture:"thunderstorm.jpg"),
        English(name:"Tissue",cht:"衛生紙", speak:"ˈtiSHo͞o", picture:"tissue.jpg"),
        English(name:"Topics",cht:"熱帶", speak:"ˈtäpik", picture:"topics.jpg"),
        English(name:"Tourist",cht:"觀光客", speak:"ˈto͝orəst", picture:"tourist.jpg"),
        English(name:"Township",cht:"鄉", speak:"ˈtounˌSHip", picture:"township.jpg"),
        English(name:"Track",cht:"軌道", speak:"trak", picture:"track.jpg"),
        English(name:"Trademark",cht:"商標", speak:"ˈtrādˌmärk", picture:"trademark.jpg"),
        English(name:"Transfusion",cht:"輸血", speak:"ˌtran(tsˈfyo͞oZHən", picture:"transfusion.jpg"),
        English(name:"Translator",cht:"翻譯機", speak:"ˈtransˌlādər", picture:"translator.jpg"),
        English(name:"Tribe",cht:"部落", speak:"trīb", picture:"tribe.jpg"),
        English(name:"Trolley",cht:"手推車", speak:"ˈträlē", picture:"trolley.jpg"),
        English(name:"Trousers",cht:"褲子", speak:"ˈtrouzərz", picture:"trousers.jpg"),
        English(name:"Trout",cht:"鮭魚", speak:"trout", picture:"trout.jpg"),
        English(name:"Trumpet",cht:"喇叭", speak:"ˈtrəmpət", picture:"trumpet.jpg"),
        English(name:"Tuxedo",cht:"燕尾服", speak:"təkˈsēdō", picture:"tuxedo.jpg"),
        English(name:"Typhoon",cht:"颱風", speak:"tīˈfo͞on", picture:"typhoon.jpg"),
        English(name:"Tyre",cht:"輪胎", speak:"tī(ər", picture:"tyre.jpg"),
        English(name:"umbrella",cht:"雨傘", speak:"ʌmˋbrɛlə", picture:"umbrella.jpg"),
        English(name:"ump",cht:"裁判", speak:"ʌmp", picture:"ump.jpg"),
        English(name:"uncle",cht:"叔叔", speak:"ˋʌŋk!", picture:"uncle.jpg"),
        English(name:"undershirt",cht:"汗衫", speak:"ˋʌndɚ͵ʃɝt", picture:"undershirt.jpg"),
        English(name:"undertaker",cht:"企業家", speak:"͵ʌndɚˋtekɚ", picture:"undertaker.jpg"),
        English(name:"undoing",cht:"毀滅", speak:"ʌnˋduɪŋ", picture:"undoing.jpg"),
        English(name:"unguent",cht:"藥膏", speak:"ˋʌŋgwənt", picture:"unguent.jpg"),
        English(name:"unicorn",cht:"獨角獸", speak:"ˋjunɪ͵kɔrn", picture:"unicorn.png"),
        English(name:"universe",cht:"宇宙", speak:"ˋjunə͵vɝs", picture:"universe.jpg"),
        English(name:"university",cht:"大學", speak:"͵junəˋvɝsətɪ", picture:"university.jpg"),
        English(name:"untruth",cht:"謊言", speak:"ʌnˋtruθ", picture:"untruth.jpg"),
        English(name:"update",cht:"更新", speak:"ʌpˋdet", picture:"update.jpg"),
        English(name:"upland",cht:"高地", speak:"ˋʌplənd", picture:"upland.jpg"),
        English(name:"upraise",cht:"舉起", speak:"ʌpˋrez", picture:"upraise.jpg"),
        English(name:"uprear",cht:"扶養", speak:"ʌpˋrɪr", picture:"uprear.jpg"),
        English(name:"uprising",cht:"叛亂", speak:"ˋʌp͵raɪzɪŋ", picture:"uprising.jpg"),
        English(name:"uproar",cht:"擾亂", speak:"ˋʌp͵ror", picture:"uproar.jpg"),
        English(name:"upset",cht:"難過的", speak:"ʌpˋsɛt", picture:"upset.jpg"),
        English(name:"upshot",cht:"結尾", speak:"ˋʌp͵ʃɑt", picture:"upshot.jpg"),
        English(name:"upswing",cht:"上升", speak:"ˋʌp͵swɪŋ", picture:"upswing.jpg"),
        English(name:"uptown",cht:"住宅區", speak:"ˋʌpˋtaʊn", picture:"uptown.jpg"),
        English(name:"uranium",cht:"軸", speak:"jʊˋrenɪəm", picture:"uranium.jpg"),
        English(name:"Uranus",cht:"天王星", speak:"ˋjʊərənəs", picture:"Uranus.jpg"),
        English(name:"urchin",cht:"小孩", speak:"ˋɝtʃɪn", picture:"urchin.png"),
        English(name:"urinal",cht:"尿壺", speak:"ˋjʊrən!", picture:"urinal.jpg"),
        English(name:"urn",cht:"甕", speak:"ɝn", picture:"urn.jpg"),
        English(name:"user",cht:"用戶", speak:"ˋjuzɚ", picture:"user.png"),
        English(name:"usher",cht:"招待員", speak:"ˋʌʃɚ", picture:"usher.jpg"),
        English(name:"ute",cht:"載貨汽車", speak:"juːt", picture:"ute.jpg"),
        English(name:"uterus",cht:"子宮", speak:"ˋjutərəs", picture:"uterus.jpg"),
        English(name:"valentine",cht:"情人", speak:"ˋvæləntaɪn", picture:"valentine.jpg"),
        English(name:"valise",cht:"旅行袋", speak:"vəˋlis", picture:"valise.jpg"),
        English(name:"valley",cht:"山谷", speak:"ˋvælɪ", picture:"valley.jpg"),
        English(name:"vampire",cht:"吸血鬼", speak:"ˋvæmpaɪr", picture:"vampire.jpg"),
        English(name:"van",cht:"運貨車", speak:"væn", picture:"van.jpg"),
        English(name:"vanguard",cht:"先鋒", speak:"ˋvæn͵gɑrd", picture:"vanguard.jpg"),
        English(name:"vanilla",cht:"香草", speak:"vəˋnɪlə", picture:"vanilla.jpg"),
        English(name:"vapor",cht:"蒸氣", speak:"ˋvepɚ", picture:"vapor.jpg"),
        English(name:"varlet",cht:"惡棍", speak:"ˋvɑrlɪt", picture:"varlet.jpg"),
        English(name:"vaseline",cht:"凡士林", speak:"ˋvæs!͵in", picture:"vaseline.jpg"),
        English(name:"vassal",cht:"諸侯", speak:"ˋvæs!", picture:"vassal.jpg"),
        English(name:"vaudeville",cht:"雜耍", speak:"ˋvodə͵vɪl", picture:"vaudeville.jpg"),
        English(name:"vegetable",cht:"蔬菜", speak:"ˋvɛdʒətəb!", picture:"vegetable.jpg"),
        English(name:"vegetation",cht:"植物", speak:"͵vɛdʒəˋteʃən", picture:"vegetation.jpg"),
        English(name:"veil",cht:"面紗", speak:"vel", picture:"veil.jpg"),
        English(name:"vein",cht:"靜脈", speak:"ven", picture:"vein.jpg"),
        English(name:"velvet",cht:"絲絨", speak:"ˋvɛlvɪt", picture:"velvet.jpg"),
        English(name:"vendor",cht:"小販", speak:"ˋvɛndɚ", picture:"vendor.png"),
        English(name:"vest",cht:"馬甲", speak:"vɛst", picture:"vest.jpg"),
        English(name:"vestment",cht:"官服", speak:"ˋvɛstmənt", picture:"vestment.jpg"),
        English(name:"vesture",cht:"衣服", speak:"ˋvɛstʃɚ", picture:"vesture.jpg"),
        English(name:"vetch",cht:"野豌豆", speak:"vɛtʃ", picture:"vetch.jpg"),
        English(name:"viaduct",cht:"高架橋", speak:"ˋvaɪə͵dʌkt", picture:"viaduct.jpg"),
        English(name:"villa",cht:"別墅", speak:"ˋvɪlə", picture:"villa.jpg"),
        English(name:"villain",cht:"壞人", speak:"ˋvɪlən", picture:"villain.png"),
        English(name:"vinegar",cht:"醋", speak:"ˋvɪnɪgɚ", picture:"vinegar.jpg"),
        English(name:"viola",cht:"中提琴", speak:"vɪˋolə", picture:"viola.jpg"),
        English(name:"violin",cht:"小提琴", speak:"͵vaɪəˋlɪn", picture:"violin.jpg"),
        English(name:"violoncello",cht:"大提琴", speak:"͵viəlɑnˋtʃɛlo", picture:"violoncello.jpg"),
        English(name:"volcano",cht:"火山", speak:"vɑlˋkeno", picture:"volcano.jpg"),
        English(name:"wafer",cht:"晶片", speak:"ˋwefɚ", picture:"wafer.jpg"),
        English(name:"waft",cht:"漂浮", speak:"wæft", picture:"waft.jpg"),
        English(name:"wag",cht:"搖擺", speak:"wæg", picture:"wag.jpg"),
        English(name:"wage",cht:"工資", speak:"wedʒ", picture:"wage.jpg"),
        English(name:"wager",cht:"賭", speak:"ˋwedʒɚ", picture:"wager.jpg"),
        English(name:"wagon",cht:"敞篷車廂", speak:"ˋwægən", picture:"wagon.jpg"),
        English(name:"wagoner",cht:"車夫", speak:"ˋwægənɚ", picture:"wagoner.jpg"),
        English(name:"waist",cht:"腰", speak:"west", picture:"waist.jpg"),
        English(name:"waistcoat",cht:"背心", speak:"ˋwest͵kot", picture:"waistcoat.jpg"),
        English(name:"wallet",cht:"皮包", speak:"ˋwɑlɪt", picture:"wallet.jpg"),
        English(name:"walnut",cht:"胡桃", speak:"ˋwɔlnət", picture:"walnut.jpg"),
        English(name:"walrus",cht:"海象", speak:"ˋwɔlrəs", picture:"walrus.jpg"),
        English(name:"wand",cht:"棒", speak:"wɑnd", picture:"wand.jpg"),
        English(name:"war",cht:"戰爭", speak:"wɔr", picture:"war.jpg"),
        English(name:"ward",cht:"病房", speak:"wɔrd", picture:"ward.jpg"),
        English(name:"warden",cht:"典獄長", speak:"ˋwɔrdn", picture:"warden.jpg"),
        English(name:"wardrobe",cht:"衣櫥", speak:"ˋwɔrd͵rob", picture:"wardrobe.jpg"),
        English(name:"warehouse",cht:"倉庫", speak:"ˋwɛr͵haʊs", picture:"warehouse.jpg"),
        English(name:"warrior",cht:"武士", speak:"ˋwɔrɪɚ", picture:"warrior.jpg"),
        English(name:"warship",cht:"軍艦", speak:"ˋwɔr͵ʃɪp", picture:"warship.jpg"),
        English(name:"washboard",cht:"洗衣板", speak:"ˋwɑʃ͵bord", picture:"washboard.jpg"),
        English(name:"washroom",cht:"洗手間", speak:"ˋwɑʃ͵rum", picture:"washroom.jpg"),
        English(name:"wasp",cht:"黃蜂", speak:"wɑsp", picture:"wasp.jpg"),
        English(name:"watch",cht:"手錶", speak:"wɑtʃ", picture:"watch.jpg"),
        English(name:"water",cht:"水", speak:"ˋwɔtɚ", picture:"water.jpg"),
        English(name:"waterfall",cht:"瀑布", speak:"ˋwɔtɚ͵fɔl", picture:"waterfall.jpg"),
        English(name:"waterman",cht:"漁夫", speak:"ˋwɔtɚmən", picture:"waterman.jpg"),
        English(name:"wave",cht:"波", speak:"wev", picture:"wave.jpg"),
        English(name:"wayfarer",cht:"旅客", speak:"ˋwe͵fɛrɚ", picture:"wayfarer.jpg"),
        English(name:"weapon",cht:"武器", speak:"ˋwɛpən", picture:"weapon.jpg"),
        English(name:"xerophyte",cht:"旱生植物", speak:"ˋzɪrə͵faɪt", picture:"xerophyte.jpg"),
        English(name:"Xerox",cht:"複印", speak:"ˋzɪrɑks", picture:"Xerox.jpg"),
        English(name:"X'mas",cht:"聖誕節", speak:"ˋkrɪsməs", picture:"X'mas.jpg"),
        English(name:"X-ray",cht:"X光", speak:"ˋɛksˋre", picture:"X-ray.jpg"),
        English(name:"xylem",cht:"木質部", speak:"ˋzaɪlɛm", picture:"xylem.jpg"),
        English(name:"xylophone",cht:"木琴", speak:"ˋzaɪlə͵fon", picture:"xylophone.jpg"),
        English(name:"yacht",cht:"快艇", speak:"jɑt", picture:"yacht.jpg"),
        English(name:"yak",cht:"犛牛", speak:"jæk", picture:"yak.jpg"),
        English(name:"yam",cht:"山藥", speak:"jæm", picture:"yam.jpg"),
        English(name:"yarn",cht:"紗線", speak:"jɑrn", picture:"yarn.jpg"),
        English(name:"yawl",cht:"小艇", speak:"jɔl", picture:"yawl.jpg"),
        English(name:"yawn",cht:"呵欠", speak:"jɔn", picture:"yawn.jpg"),
        English(name:"yeast",cht:"泡沫", speak:"jist", picture:"yeast.jpg"),
        English(name:"yellow",cht:"黃色", speak:"ˋjɛlo", picture:"yellow.png"),
        English(name:"yelp",cht:"叫喊", speak:"jɛlp", picture:"yelp.jpg"),
        English(name:"yeoman",cht:"自耕農", speak:"ˋjomən", picture:"yeoman.jpg"),
        English(name:"yew",cht:"紫杉", speak:"ju", picture:"yew.jpg"),
        English(name:"yoghurt",cht:"優酪乳", speak:"ˋjogɚt", picture:"yoghurt.jpg"),
        English(name:"yogurt",cht:"優格", speak:"ˋjogɚt", picture:"yogurt.jpg"),
        English(name:"yoke",cht:"枷鎖", speak:"jok", picture:"yoke.jpg"),
        English(name:"yokefellow",cht:"同事", speak:"ˋjok͵fɛlo", picture:"yokefellow.jpg"),
        English(name:"yokel",cht:"鄉巴佬", speak:"ˋjok!", picture:"yokel.jpg"),
        English(name:"yolk",cht:"蛋黃", speak:"jok", picture:"yolk.png"),
        English(name:"young",cht:"年輕的", speak:"jʌŋ", picture:"young.jpg"),
        English(name:"yo-yo",cht:"溜溜球", speak:"ˋjo͵jo", picture:"yo-yo.jpg"),
        English(name:"yule",cht:"聖誕節", speak:"jul", picture:"yule.jpg"),
        English(name:"zany",cht:"小丑", speak:"ˋzenɪ", picture:"zany.png"),
        English(name:"zealot",cht:"狂熱分子", speak:"ˋzɛlət", picture:"zealot.jpg"),
        English(name:"zebra",cht:"斑馬", speak:"ˋzibrə", picture:"zebra.jpg"),
        English(name:"zebu",cht:"犛牛", speak:"ˋzibju", picture:"zebu.jpg"),
        English(name:"zenith",cht:"天頂", speak:"ˋzinɪθ", picture:"zenith.jpg"),
        English(name:"zephyr",cht:"和風", speak:"ˋzɛfɚ", picture:"zephyr.jpg"),
        English(name:"zero",cht:"零", speak:"ˋzɪro", picture:"zero.jpg"),
        English(name:"Zeus",cht:"宙斯", speak:"zjus", picture:"Zeus.jpg"),
        English(name:"zion",cht:"天國", speak:"ˋzaɪən", picture:"zion.jpg"),
        English(name:"zip",cht:"拉鍊", speak:"zɪp", picture:"zip.png"),
        English(name:"zipcode",cht:"郵政編號", speak:"zɪp kod", picture:"zipcode.jpg"),
        English(name:"zipper",cht:"拉鍊", speak:"ˋzɪpɚ", picture:"zipper.jpg"),
        English(name:"zither",cht:"箏", speak:"ˋzɪθɚ", picture:"zither.jpg"),
        English(name:"zodiac",cht:"十二宮圖", speak:"ˋzodɪ͵æk", picture:"zodiac.jpg"),
        English(name:"zone",cht:"地區", speak:"zon", picture:"zone.jpg"),
        English(name:"zoo",cht:"動物園", speak:"zu", picture:"zoo.jpg"),
        English(name:"zygote",cht:"受精卵", speak:"ˋzaɪgot", picture:"zygote.jpg")
    ]
        
        
    override var prefersStatusBarHidden: Bool{
        return true
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let string = "Hello"
//        let synthesizer = AVSpeechSynthesizer()
//        let voice = AVSpeechSynthesisVoice(language: "en_US")
//        let utterance = AVSpeechUtterance(string: string)
//        utterance.voice = voice
//        synthesizer.speak(utterance)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
      getWord()
        
    }
    

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return englishs.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell{
        let cellid = "eng"
        let cell=tableView.dequeueReusableCell(withIdentifier:cellid , for: indexPath) as! engTableViewCell
        cell.namelabel.text=englishs[indexPath.row].name
        cell.wwlabel.text=englishs[indexPath.row].cht
        cell.aalabel.text=englishs[indexPath.row].speak
        cell.pictures.image=UIImage(named: englishs[indexPath.row].picture)
        return cell
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "englishshow"{
            if let indexPath = tableView.indexPathForSelectedRow{
                let destinationController = segue.destination
                    as! englishViewController
                    destinationController.englishs = englishs[indexPath.row]
                
                
            }
            
            
        }
    }
    //    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //        let optionMenu = UIAlertController(title:nil,message :"what you want to do",preferredStyle:.actionSheet)
    //        let cancelAction = UIAlertAction(title:"cancel",style:.cancel,handler:nil)
    //        optionMenu.addAction(cancelAction)
    //        self.present(optionMenu,animated: true,completion: nil)
    //        let speakActionHandler={(action:UIAlertAction!)->Void in
    //            let string=self.names["eng"]?[indexPath.row]
    //            let synthesizer=AVSpeechSynthesizer()
    //            let voice=AVSpeechSynthesisVoice(language:"en-US")
    //            let utterance = AVSpeechUtterance(string:string!)
    //            utterance.voice=voice
    //            synthesizer.speak(utterance)
    //        }
    //        let englishWords :String = (self.names["eng"]?[indexPath.row])!
    //        let speakAction=UIAlertAction(title:"Speak Word -> \(englishWords)",style:.default,handler:speakActionHandler)
    //        optionMenu.addAction(speakAction)
    //    }
    
    /* override func tableView(_ tableView:UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath){
     
     if editingStyle == .delete{
     
     names["eng"]?.remove(at: indexPath.row)
     names["cht"]?.remove(at: indexPath.row)
     names["speak"]?.remove(at: indexPath.row)
     names["picture"]?.remove(at: indexPath.row)
     
     
     }
     tableView.reloadData()
     print("Total item: \(names.count)")
     for name in names{
     
     print(name)
     }
     }*/
    
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        
        let shareAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title:"share", handler: {(action,  indexPath) -> Void in
            
            let defaultText = "Just checking in at" + self.englishs[indexPath.row].name
            let activityController = UIActivityViewController(activityItems: [defaultText], applicationActivities: nil)
            self.present(activityController, animated: true, completion: nil)
        })
        
        
        let deleteAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title:"DELETE")
        {(action, indexPath) -> Void in
            
            self.englishs.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .fade)
            
            
            
        }
        let speakAction =  UITableViewRowAction(style: UITableViewRowActionStyle.default, title:"speak")
        {(action, indexPath) -> Void in
            let string=self.englishs[indexPath.row].name
            let synthesizer=AVSpeechSynthesizer()
            let voice=AVSpeechSynthesisVoice(language:"en-US")
            let utterance = AVSpeechUtterance(string:string)
            utterance.voice=voice
            synthesizer.speak(utterance)
            
            
        }
        
        return [deleteAction, shareAction, speakAction]
        
    }
    
    
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
     
}
